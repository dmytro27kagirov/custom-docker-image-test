FROM arm64v8/ubuntu:22.04

RUN apt-get -f install

RUN apt-get update && \
      apt-get -y install sudo

RUN useradd -m docker && echo "docker:docker" | chpasswd && adduser docker sudo

USER docker
CMD /bin/bash

RUN pwd && ls && uname --all && dpkg --version
RUN echo "docker" | sudo -S apt-get -y install pytest
