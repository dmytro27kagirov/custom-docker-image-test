import QtCore
import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs
import QtQuick.Particles
import Qt.labs.folderlistmodel


Window {
    id: window
    visible: true
    title: qsTr("Cute Aquarium")
    Component.onCompleted: {
        //window.showFullScreen();  // Opens the window to full screen
        window.showMaximized();
    }

    property var config: ({})       // Configuration object

    // Sets the background for the aquarium. Takes image as argument
    function setBackground(selectedFile){
        backgroundImage.source = selectedFile
        closeHandle.focus = true
    }

    // Add fish to the aquarium. Takes image as argument
    function addFish(selectedFile) {
        var fishComponent = Qt.createComponent("FishComponent.qml")
        var fishObject = undefined
        if (fishComponent.status === Component.Ready) {
            fishObject = fishComponent.createObject(window)
            fishObject.imgSource = selectedFile
        }
        // Error Handling
        if (fishComponent === null)
            console.log("Error creating fish object");

        closeHandle.focus = true
        return fishObject
    }

    // This method is called from the main.cpp and save the configuration settings
    // from the app.config file to the this.config object
    function setConfiguration(fishWidth, fishHeight, fishMinDuration, fishMaxDuration, fishDeltaWave, bubbleLifeSpan, bubbleMainSize) {
        config.fishWidth = fishWidth
        config.fishHeight = fishHeight
        config.fishMinDuration = fishMinDuration
        config.fishMaxDuration = fishMaxDuration
        config.fishDeltaWave = fishDeltaWave
        config.bubbleLifeSpan = bubbleLifeSpan
        config.bubbleMainSize = bubbleMainSize
    }

    // Creates bubbles source at x coordinate
    function createBubbles(xPos){
        var bubbleComponent = Qt.createComponent("BubbleParticles.qml")
        if (bubbleComponent.status === Component.Ready) {
            let bubble = bubbleComponent.createObject(window)
            bubble.xPos = xPos
        }
        // Error Handling
        if (bubbleComponent === null)
            console.log("Error creating bubble object");
    }

    // Checks the fish folder and the background folder every second.
    // If new fish appear, then add them to the aquarium
    FishBgUpdate{}

    // Pressing the Esc key close the window
    Item {
        id: closeHandle
        focus: true
        Keys.onEscapePressed: window.close()
    }

    // Executed once. Need to create two sources of bulbs after the config file is read
    Timer {
        interval: 100; running: true; repeat: false
        onTriggered: {
            createBubbles(window.width - window.width/5);
            createBubbles(window.width - (window.width*3)/5);
        }
    }

    Image {
        id: backgroundImage
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
    }
}
