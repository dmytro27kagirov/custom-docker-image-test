import QtQuick
import QtQuick.Controls


Image {
    id: fish
    z: 1
    width: config.fishWidht
    height: config.fishHeight
    fillMode: Image.PreserveAspectFit
    x: pathInterpolator.x - width/2
    y: pathInterpolator.y - height/2
    rotation: (mirrorFish)? -pathInterpolator.angle : pathInterpolator.angle
    source: "assets/transparent.png"

    // FishBgUpdate.qml will assign a fish image file to this variable
    property string imgSource: "assets/transparent.png"

    property bool mirrorFish: false
    property real mirrorK: (mirrorFish)? -1 : 1

    // scale coefficient of fish
    property real scaleK: 1
    // shows the fish is growing or decreasing
    property bool isIncrease: false

    // Trajectory of the fish depends on this parameter. +-deltaWave to Y position
    readonly property int deltaWave: config.fishDeltaWave
    // Minimum animation duration (fish speed)
    readonly property int minDuration: config.fishMinDuration
    // Maximum animation duration (fish speed)
    readonly property int maxDuration: config.fishMaxDuration

    property var path: pathInterpolator.path
    property int duration: Math.floor(minDuration + Math.random()*maxDuration)
    property variant startPos: [50, Math.floor(50 + Math.random()*(window.height-50))]
    property variant endPos: [window.width-50, getEndPosY()]

    function getEndPosY(){
        if(startPos[1] + deltaWave*2 > window.height)
            return startPos[1] - deltaWave*2
        else if(startPos[1] - deltaWave*2 <= 0)
            return startPos[1] + deltaWave*2
        else{
            // the final position will be less (-1), the same (0) or greater (1)
            var k = [-1, 0, 1]
            var randomK = k[Math.floor(Math.random()*k.length)]
            return startPos[1] + (deltaWave * 2 * randomK)
        }
    }

    function getWavePosX(part){
        return (startPos[0]+endPos[0]) * part
    }

    function getWavePosY(part){
        var posY = part * (endPos[1] - startPos[1]) + startPos[1]
        if(posY + deltaWave > window.height)
            posY -= deltaWave
        else if(posY - deltaWave <= 0)
            posY += deltaWave
        else
            posY = posY + Math.floor(Math.random()*deltaWave*2 - deltaWave)
        return posY
    }

    Image{
        id: fishImage
        visible: false
        source: fish.imgSource
        width: fish.width
        height: fish.height
        fillMode: Image.PreserveAspectFit
    }

    ShaderEffectSource {
        id: theSource
        sourceItem: fishImage
    }

    ShaderEffect {
        width: fish.width
        height: fish.height
        property variant source: theSource
        property real amplitude: 0.03
        property real frequency: 5
        property real time: 0
        NumberAnimation on time { loops: Animation.Infinite; from: 0; to: Math.PI * 2; duration: fish.duration/15 }
        fragmentShader: "assets/shaders/wobble.frag.qsb"
    }

    // Mirrors a fish when it turns and set z-index to fish
    Timer {
        interval: 100; running: true; repeat: true
        property real lastScaleK: scaleK;
        onTriggered: {
            if(pathInterpolator.angle > 90.0 && pathInterpolator.angle < 270.0){
                fish.transform = [translate, scale, zRotation]
                mirrorFish = true
            } else{
                fish.transform = [scale, zRotation]
                mirrorFish = false
            }

            // set the correct z-index to fish
            if(scale.xScale < 1.1)
                fish.z = 1
            else if(scale.xScale < 1.2)
                fish.z = 2
            else if(scale.xScale < 1.3)
                fish.z = 3
            else if(scale.xScale < 1.4)
                fish.z = 4
            else if(scale.xScale < 1.5)
                fish.z = 5
            else if(scale.xScale < 1.6)
                fish.z = 6
            else if(scale.xScale < 1.7)
                fish.z = 7
            else if(scale.xScale < 1.8)
                fish.z = 8
            else if(scale.xScale < 1.9)
                fish.z = 9
            else if(scale.xScale <= 2)
                fish.z = 10

           // Set the correct isIncrease value
           isIncrease = scaleK > lastScaleK
           lastScaleK = scaleK
        }
    }

    Translate { id: translate; y: -fish.height }
    Scale {
        id: scale;
        xScale: 1*scaleK;
        yScale: 1*scaleK*mirrorK
    }
    Rotation {
        id: zRotation;
        axis { x: 0; y: 1; z: 0 }

        property real c: Math.sqrt(Math.pow(fish.height, 2) + Math.pow(fish.width, 2))/2
        origin.y: zRotation.c * Math.sin(Math.acos((2*zRotation.c*zRotation.c - Math.pow(fish.height, 2))/(2*zRotation.c*zRotation.c))/2 + pathInterpolator.angle*Math.PI/180)

        property real angleK: -(Math.cos(Math.PI*2*((scaleK-1)/(1.5-1)))-1)/2     // easeInOutSine function
        angle: 30*angleK*mirrorK*((isIncrease)? -1 : 1)
    }

    SequentialAnimation{
        running: true
        loops: Animation.Infinite

        PropertyAnimation{
            target: fish
            easing.type: Easing.InOutSine
            properties: "scaleK"
            from: 1
            to: 1.5
            // random duration from 5 to 12 seconds
            duration: Math.floor(5000 + Math.random()*12000)
        }
        PropertyAnimation{
            target: fish
            easing.type: Easing.InOutSine
            properties: "scaleK"
            from: 1.5
            to: 1
            // random duration from 5 to 12 seconds
            duration: Math.floor(5000 + Math.random()*12000)
        }
    }

    PathInterpolator {
        id: pathInterpolator

        path: Path {
            startX: startPos[0]; startY: startPos[1]

            PathCurve { x: getWavePosX(1/4); y: getWavePosY(1/4) }
            PathCurve { x: getWavePosX(2/4); y: getWavePosY(2/4) }
            PathCurve { x: getWavePosX(3/4); y: getWavePosY(3/4) }

            PathCurve { x: endPos[0]; y: endPos[1] }

            PathCurve { x: getWavePosX(3/4); y: getWavePosY(3/4) }
            PathCurve { x: getWavePosX(2/4); y: getWavePosY(2/4) }
            PathCurve { x: getWavePosX(1/4); y: getWavePosY(1/4) }

            PathCurve { x: startPos[0]; y: startPos[1] }
        }

        NumberAnimation on progress {
            id: fishAnimation
            running: true
            duration: fish.duration
            loops: Animation.Infinite
            from: 0
            to: 1
        }
    }
}
