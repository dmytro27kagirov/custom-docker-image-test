QT += quick

SOURCES += \
        main.cpp

# Sets the build directory
DESTDIR = ../CuteAquariumBuild

# Copies the configuration file, assets folder to the project build directory and compile shader wobble.frag
win32 {
    system(if not exist "..\CuteAquariumBuild" mkdir "..\CuteAquariumBuild")
    system(qsb --glsl "100es,120,150" --hlsl 50 --msl 12 -o assets/shaders/wobble.frag.qsb assets/shaders/wobble.frag)
    system(copy app.config ..\CuteAquariumBuild\app.config)
    system(if not exist "..\CuteAquariumBuild\assets" mkdir ..\CuteAquariumBuild\assets)
    system(if not exist "..\CuteAquariumBuild\assets\fishes" Xcopy /E /I assets\fishes ..\CuteAquariumBuild\assets\fishes)
    system(if not exist "..\CuteAquariumBuild\assets\background" Xcopy /E /I assets\background ..\CuteAquariumBuild\assets\background)
}
unix{
    system(qsb --glsl "100es,120,150" --hlsl 50 --msl 12 -o assets/shaders/wobble.frag.qsb assets/shaders/wobble.frag)
    system(mkdir -p ../CuteAquariumBuild)
    system(cp app.config ../CuteAquariumBuild)
    system(mkdir -p ../CuteAquariumBuild/assets)
    system(cp -r assets/fishes ../CuteAquariumBuild/assets)
    system(cp -r assets/background ../CuteAquariumBuild/assets)
}
macx {
    # Need to add commands to copy app.config, assets folder and compile shader wobble.frag for MacOS
}

resources.files = main.qml FishComponent.qml BubbleParticles.qml FishBgUpdate.qml \
    assets/bubbles/bubble.png assets/transparent.png \
    assets/shaders/wobble.frag.qsb
resources.prefix = /$${TARGET}
RESOURCES += resources \

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    BubbleParticles.qml \
    FishBgUpdate.qml \
    main.qml \
    FishComponent.qml

HEADERS +=
